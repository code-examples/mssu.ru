# [mssu.ru](https://mssu.ru) source codes

<br/>

### Run mssu.ru on localhost

    # vi /etc/systemd/system/mssu.ru.service

Insert code from mssu.ru.service

    # systemctl enable mssu.ru.service
    # systemctl start mssu.ru.service
    # systemctl status mssu.ru.service

http://localhost:4064
